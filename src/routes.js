import React from 'react';
//import Registro_personas from './views/Registros/Registro_Personas/registro_personas';

const Compra = React.lazy(() => import('./views/Carros/Compra_Carro'));
const Venta = React.lazy(() => import('./views/Carros/Venta_Carro'));
const Registro_Carro = React.lazy(() => import('./views/Carros/Ingresar_Carro'));
const Publicaciones = React.lazy(() => import('./views/Carros/Publicaciones'));
const Dashboard = React.lazy(() => import('./views/Dashboard'));
const Registro_Personas = React.lazy(()=> import('./views/Registros/Registro_Personas/registro_personas'));
const Users = React.lazy(() => import('./views/Users/Users'));
const User = React.lazy(() => import('./views/Users/User'));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/Carros/Compra_Carro', exact: true, name: 'Compra', component: Compra },
  { path: '/Carros/Ingresar_Carro', exact: true, name: 'Registro Carro', component: Registro_Carro },
  { path: '/Carros/Venta_Carro',    exact:true, name: 'Venta', component: Venta },
  { path: '/Publicaciones/Publicaciones',exact:true, name: 'Publicaciones',component: Publicaciones },
  { path: '/Registro_Personas/registro_personas' ,exact:true ,name: 'Registro Personas', component: Registro_Personas },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
];

export default routes;
