export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'fa fa-university',
      badge: {
        variant: 'info'
      },
    },
    {
      title: true,
      name: 'Opciones',
      wrapper: {
        element: '',
        attributes: {},
      },
    },
    {
      name: 'Carros',
      url: '/Carros',
      icon: 'fa fa-car',
      children: [
        {
          name: 'Compra',
          url: '/Carros/Compra_Carro',
          icon: 'fa fa-pencil',
        },
        {
          name: 'Venta',
          url: '/Carros/Venta_Carro',
          icon: 'fa fa-pencil',
        },
        {
          name: 'Registrar Carro',
          url: '/Carros/Ingresar_Carro',
          icon: 'fa fa-pencil',
        
        }
 
      ],
    },
    {
      name: 'Publicaciones',
      url: '/Publicaciones/Publicaciones',
      icon: 'fa fa-newspaper-o'
    },
    {
      name: 'Registrar',
      url: '/buttons',
      icon:  'fa fa-id-card',
      children: [
        {
          name: 'Registrar Personas',
          url:  'Registro_Personas/registro_personas',
          icon: 'fa fa-pencil',
        },
        {
          name: 'Button dropdowns',
          url: '/buttons/button-dropdowns',
          icon: 'fa fa-pencil',
        },
      ],
    },
   
    
  ],
};
