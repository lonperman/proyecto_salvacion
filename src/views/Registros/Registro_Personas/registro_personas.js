import React, { Component, useState } from 'react';
import { Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import {personaService} from '../../service/Persona.service';

const Registro_personas = () => {

  const[nombre_completo,setnombre_completo] = useState('');
  const[numero_documento,setnumero_documento] = useState('');
  const[correo,setcorreo] = useState('');
  const[id_tipo_documento,setid_tipo_documento] = useState('');
  const[telefono,settelefono] = useState('');
  const[direccion,setdireccion] = useState('');


  const datos = {
      'nombre_completo': nombre_completo,
      'numero_documento' : numero_documento,
      'correo' : correo,
      'id_tipo_documento': id_tipo_documento,      
      'telefono': telefono,
      'direccion': direccion
  }
console.log();

const registrar = async (e) => {
  e.preventDefault();

  personaService.registroAll(nombre_completo,numero_documento,correo,id_tipo_documento,
    telefono,direccion).then(user => {
    console.log(user);
  })
  .catch( err => {
    console.log(err);
  })
}
    return (
      <div className="animated fadeIn">
        <Container>
          <Row >
            <Col xs="12" sm="6" >
              <Card className="mx-4">
                <CardBody className="p-4">
                    <h1>Registro Personas</h1>
                    <p className="text-muted">Crear Persona</p>
                    <Form onSubmit={registrar}>  
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setnombre_completo(e.target.value)} value={nombre_completo} type="text" placeholder="Nombre Completo" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-address-book"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setnumero_documento(e.target.value)} value={numero_documento} type="number" placeholder="Documento" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setcorreo(e.target.value)} value={correo} type="text" placeholder="Email" autoComplete="email" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-address-card"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setid_tipo_documento(e.target.value)} value={id_tipo_documento} type="text" placeholder="Tipo Documento" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-volume-control-phone"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => settelefono(e.target.value)} value={telefono} type="text" placeholder="Telefono" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-building"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setdireccion(e.target.value)} value={direccion} type="text" placeholder="Direccion" autoComplete="username" />
                    </InputGroup>
                    
                    <Button type="submit" color="success" block>Guardar Persona</Button>
                  </Form>
                </CardBody>
            
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }


export default Registro_personas;
