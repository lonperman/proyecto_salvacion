import React from 'react';
import ReactDOM from 'react-dom';
import Registro_personas from './registro_personas';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Registro_personas />, div);
  ReactDOM.unmountComponentAtNode(div);
});
