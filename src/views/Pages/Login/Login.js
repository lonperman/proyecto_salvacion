import React, { useState } from 'react';
import authenticationService from '../../service/authentication.service';
import {withRouter } from 'react-router-dom';
import {useAuthentication} from '../../session/AuthenticationProvider';
import {Button,
   Card, CardBody,
    CardGroup, Col,
    Container,
    Form,
    Input,
    InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
 
  

 

 const Login = ({history}) => {

  const[username, setusername] = useState('');
  const[password,setPassword] = useState('');

  const auth = useAuthentication();

  const login = async (e) => {

   
    e.preventDefault();
    authenticationService.signIn(username,password).then(user => {
      console.log(user);
      auth.login(user);
      history.push('/');
    })
    .catch( err => {
      console.log(err);
    })
    }


      return(
        <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form onSubmit={ login }>
                      <h1>Login</h1>
                      <p className="text-muted">Login</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input onChange={e => setusername(e.target.value)} value={username} type="text" name="username" placeholder="Username" autoComplete="username" />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input onChange={e => setPassword(e.target.value)} value={password} type="password" name="password" placeholder="Password" autoComplete="current-password" />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">Login</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>                  
      );
  }
  export default withRouter(Login);

  