import React from 'react';
import { Route} from 'react-router-dom';
import Login from '../Pages/Login/Login';
import { useAuthentication } from '../session/AuthenticationProvider';

export function PrivateRoute({ component, ...options }){
    
    const { isAuthenticated } = useAuthentication();
    // const isAuthenticated = auth.isAuthenticated

    return(
        isAuthenticated 
            ? <Route {...options} component={component} />
            : <Login/>        
    )
};


export default PrivateRoute;