import axios from 'axios';
//import {authHeader} from '../helpers/auth-header';
import {handleResponse} from '../helpers/handle-response';
import authenticationService from '../service/authentication.service';
export const carroService = {
    getAll,
    postAll,
    registroAll,
    getCarros,
    postVenta_C,
    postCompra_C

    //putAll,
    //delAll

};

function getAll(){
    
   
    const token = authenticationService.getToken();
  
    const token_2 = token.slice(1, -1);
    console.log(token_2); 
 
   return new Promise( (resolve, reject) => {
     axios({
         method: 'GET',
         url: 'https://andresriascos.pw/api/v1/en_ventas',
         headers:{ 'Authorization' :`Bearer ${token_2}`}
     })
     .then(handleResponse)
     .then(data =>{resolve(data); console.log(data);})
   })
   
  
    
}

function getCarros(){
    
   
    const token = authenticationService.getToken();
  
    const token_2 = token.slice(1, -1);
   
 
   return new Promise( (resolve, reject) => {
     axios({
         method: 'GET',
         url: 'https://andresriascos.pw/api/v1/carros',
         headers:{ 'Authorization' :`Bearer ${token_2}`}
     })
     .then(handleResponse)
     .then(data =>{resolve(data); console.log(data);})
   })
   
  
    
}

function postAll(){
    const token = authenticationService.getToken();
    const token_2 = token.slice(1, -1);

    return new Promise((resolve, reject) => {
        axios({
            method: 'POST',
            url: 'https://andresriascos.pw/api/v1/carros',
            headers:{'Authorization': `Bearer ${token_2}`}})
        .then(handleResponse)
        .then(data => {
            resolve(true);
            console.log(data);
        })
    })
}

function  postVenta_C(precioVenta,ganancia,id_carro,antiguo_d,socio,nuevo_D,id_venta){
    const token = authenticationService.getToken();
    const token_2 = token.slice(1, -1);
    console.log(precioVenta,ganancia,id_carro,antiguo_d,socio,nuevo_D,id_venta)

    return new Promise((resolve, reject) => {
        axios({
            method: 'POST',
            url: 'https://andresriascos.pw/api/v1/ventas',
            headers:{'Authorization': `Bearer ${token_2}`},
            data: {
               'precio_venta': precioVenta,
               'ganancia': ganancia,
               'id_carro': id_carro,
               'antiguo_dueno': antiguo_d,
               'socio': socio,
               'nuevo_dueno': nuevo_D,
               'id_en_venta':id_venta

            }
        
        })
        .then(handleResponse)
        .then(data => {
            resolve(true);
            console.log(data);
        })
    }) 
}

function  postCompra_C(precioCompra,id_carro,propietario,persona_compra,precio_venta){
    const token = authenticationService.getToken();
    const token_2 = token.slice(1, -1);

    return new Promise((resolve, reject) => {
        axios({
            method: 'POST',
            url: 'https://andresriascos.pw/api/v1/compras',
            headers:{'Authorization': `Bearer ${token_2}`},
            data: {
               'precio_compra': precioCompra,
               'id_carro': id_carro,
               'propietario_compra': propietario,
               'persona_compra': persona_compra,
               'precio_venta' : precio_venta

            }
        
        })
        .then(handleResponse)
        .then(data => {
            resolve(true);
            console.log(data);
        })
    }) 
}


function registroAll(modelo,ano,color,serial_motor,
    serial_chasis,ciudad,placa,descripcion,img,id_marca,propietario){
   
   
        const token = authenticationService.getToken();
  
        const token_2 = token.slice(1, -1);

    return new Promise((resolve, reject) => {
        axios({
            method: 'POST',
            url: `https://andresriascos.pw/api/v1/carros `,
            headers:{'Authorization': `Bearer ${token_2}`},
            data:{

            'modelo' : modelo,
            'ano': ano,
            'color' : color,
            'serial_motor' : serial_motor,
            'serial_chasis': serial_chasis,
            'ciudad': ciudad,
            'placa': placa,
            'descripcion': descripcion,
            'img': img,
            'id_marca': id_marca,
            'propietario': propietario
            }
    })
        .then(handleResponse)
        .then(data => {
            resolve(true);
            console.log(data);
        })
    })
}