import axios from 'axios';
//import {authHeader} from '../helpers/auth-header';
import {handleResponse} from '../helpers/handle-response';
import authenticationService from '../service/authentication.service';
export const personaService = {
    getAll,
    postAll,
    registroAll,
    //putAll,
    //delAll

};

function getAll(){
    
    ///const requestOptions = {method: 'GET', headers: authHeader()};
    //console.log(requestOptions);
    const token = authenticationService.getToken();
  
    const token_2 = token.slice(1, -1);
    console.log(token_2); 
   // console.log(headers);

  // console.log(requestOptions);
   return new Promise( (resolve, reject) => {
     axios({
         method: 'GET',
         url: 'https://andresriascos.pw/api/v1/personas',
         headers:{ 'Authorization' :`Bearer ${token_2}`}
     })
     .then(handleResponse)
     .then(data =>{resolve(data); console.log(data);})
   })
   
  
    
}

function postAll(){
    const token = authenticationService.getToken();
    const token_2 = token.slice(1, -1);

    return new Promise((resolve, reject) => {
        axios({
            method: 'POST',
            url: 'https://andresriascos.pw/api/v1/personas',
            headers:{'Authorization': `Bearer ${token_2}`}})
        .then(handleResponse)
        .then(data => {
            resolve(true);
            console.log(data);
        })
    })
}



function registroAll(nombre_completo,numero_documento,correo,id_tipo_documento,
    telefono,direccion){
   
   
        const token = authenticationService.getToken();
  
        const token_2 = token.slice(1, -1);

    return new Promise((resolve, reject) => {
        axios({
            method: 'POST',
            url: `https://andresriascos.pw/api/v1/personas `,
            headers:{'Authorization': `Bearer ${token_2}`},
            data:{

            'nombre_completo': nombre_completo,
            'numero_documento' : numero_documento,
            'correo' : correo,
            'id_tipo_documento': id_tipo_documento,
            'telefono': telefono,
            'direccion': direccion
            }
    })
        .then(handleResponse)
        .then(data => {
            resolve(true);
            console.log(data);
            
        })
    })
}