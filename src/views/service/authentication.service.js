import axios from 'axios';
import { handleResponse } from '../helpers/handle-response';


class AuthenticationService {
    constructor(){
        this.getAuthUser = this.getAuthUser.bind(this);
        this.getToken = this.getToken.bind(this);
        this.isAuthenticated = this.isAuthenticated.bind(this);
        this.signIn = this.signIn.bind(this);
        this.signOut = this.signOut.bind(this);
    }

    getAuthUser(){
        const user = localStorage.getItem('user');
        return user;
    }

    getToken(){
        return localStorage.getItem('token');
    }

    isAuthenticated(){
        return this.getAuthUser() != null;
    }

    signIn(username,password){
        return new Promise((resolve,reject) => {
            axios.post('https://andresriascos.pw/api/v1/login',{'username' : username, 'password': password})
            .then(handleResponse)
            .then( data => {
                localStorage.setItem('token', JSON.stringify(data.token));
                localStorage.setItem('user', JSON.stringify(data.user));

                resolve(data.user);
            })
            .catch(err => {
                reject(null);
            })
        });
    }

    signOut() {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
    }
}

const authenticationService = new AuthenticationService();
export default authenticationService;