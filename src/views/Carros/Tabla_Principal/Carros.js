import React, { Component } from 'react';


const Carros = ({carrito}) => {
    
      
     return( 
         <tr>
             <th>{carrito.precio_venta}</th>
             <th>{carrito.socio.nombre_completo}</th>
             <th>{carrito.carro.modelo}</th>
             <th>{carrito.carro.ano}</th>
             <th>{carrito.carro.color}</th>
             <th>{carrito.carro.marca.marca}</th>
             <th>{carrito.carro.propietario_persona.nombre_completo}</th>       
        </tr>
         )
    
}

export default Carros;