import React from 'react';
import ReactDOM from 'react-dom';
import Carro_Compra from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Carro_Compra />, div);
  ReactDOM.unmountComponentAtNode(div);
});
