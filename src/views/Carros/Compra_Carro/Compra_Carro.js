import React, { useState,useEffect } from 'react';
import {carroService} from '../../service/Carro.service';
import {personaService} from '../../service/Persona.service';


import {
  Button,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
 } from 'reactstrap';

const Carro_Compra = () => {

  const[carros,setCarros] = useState([]);
  const[personas,setPersonas] = useState([]);
  const[precioCompra,setprecioCompra] = useState([]);
  const[id_carro,setid_carro] = useState([]);
  const[propietario,setpropietario] = useState([]);
  const[persona_compra,setPersona_compra] = useState([]);
  const[precio_venta,setprecio_venta] = useState([]);
 

  const id = localStorage.getItem('user');

  console.log(id);

 // const id_user = id.id_user;

  //console.log(id_user);


  const loadCarros = async () => {
      carroService.getCarros().then(carros => {
        setCarros(carros);
      });
      console.log(carros);
    }

    useEffect( () => {
      loadCarros();
    }, [])

  
 

    const loadPersonas = async () => {
      personaService.getAll().then(personas => {
        setPersonas(personas);
      });
      console.log(personas);
    }

    useEffect( () => {
      loadPersonas();
    }, [])

    const registrar_venta = async() => {
      carroService.postCompra_C(precioCompra,id_carro,propietario,persona_compra,precio_venta).then(carros => {
        setCarros(carros);
      });
      console.log(carros);
    }

  
  

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6">
            <CardHeader>
              <strong>Formulario De Compra</strong>
            </CardHeader>
            <CardBody>
              <Form onSubmit= {registrar_venta}>
              <Row>
              <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="precio_compra">Precio Compra</Label>
                    <Input onChange={e => setprecioCompra(e.target.value)} value={precioCompra} type="number" id="precio_venta"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="id_carro">Placa</Label>
                    <Input onChange={e => setid_carro(e.target.value)} value={id_carro} type="select" id="id_carro">
                     {carros.map(carrito => (
                       <option value={carrito.id_carro}>{carrito.placa}</option>  
                     ))}
                    </Input>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="propietario_compra">Propietario Compra</Label>
                    <Input onChange={e => setpropietario(e.target.value)} value={propietario} type="select" id="antiguo_dueño">
                    {carros.map(carrito => (
                      
                       <option value={carrito.propietario_persona.id_persona}  key = {carrito.id_carro}
                       >{carrito.propietario_persona.nombre_completo}</option>
                     ))}
                    </Input>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="persona_compra">Persona Compra</Label>
                    <Input onChange={e => setPersona_compra(e.target.value)} value={persona_compra} type="select" id="antiguo_dueño">
                    {carros.map(carrito => (
                      
                       <option value={carrito.propietario_persona.id_persona}  key = {carrito.id_carro}
                       >{carrito.propietario_persona.nombre_completo}</option>
                     ))}
                    </Input>
                  </FormGroup>
                  <FormGroup>
                  <Label htmlFor="precio_venta">Precio Compra</Label>
                  <Input onChange={e => setprecio_venta(e.target.value)} value={precio_venta} type="number" id="precio_venta"/>
                  </FormGroup>
                </Col>

              </Row>
              <CardFooter>
              <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i>Submit</Button>
            </CardFooter>
              </Form>
            </CardBody>
           
          </Col>
        </Row>
     
      </div>

    );
  
}

export default Carro_Compra;
