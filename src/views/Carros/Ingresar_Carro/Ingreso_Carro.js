import React, { useState,useEffect } from 'react';
import {carroService} from '../../service/Carro.service';
import {marcasService} from '../../service/Marcas.service';
import {
  Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row
 } from 'reactstrap';

const Ingreso_Carro = () =>  {

  const[modelo,setmodelo] = useState('');
  const[ano,setano]= useState('');
  const[color,setcolor] = useState('');
  const[serial_motor,setserial_motor]=useState('');
  const[serial_chasis,setserial_chasis] = useState('');
  const[ciudad,setciudad]= useState('');
  const[placa,setplaca] = useState('');
  const[descripcion,setdescripcion]= useState('');
  const[img,setimg] = useState('');
  const[id_marca,setid_marca]= useState('');
  const[propietario,setpropietario]= useState('');
 

  const datos = {
    'modelo' : modelo,
    'ano' : ano,
    'color' : color,
    'serial_motor' : serial_motor,
    'serial_chasis': serial_chasis,
    'ciudad': ciudad,
    'placa': placa,
    'descripcion':descripcion ,
    'img': img,
    'id_marca': id_marca,
    'propietario': propietario
  }

  const[marcas,setMarcas] = useState([]);

  const loadMarcas = async () => {
    marcasService.getMarcas().then(marcas => {
      setMarcas(marcas);
    });
    console.log(marcas);
  }

  useEffect( () => {
    loadMarcas();
  }, [])

  console.log();
  const registrar = async (e) => {
    e.preventDefault();

    carroService.registroAll(modelo,ano,color,serial_motor,
      serial_chasis,ciudad,placa,descripcion,img,id_marca,propietario).then(user => {
        console.log(user);
    })
    .catch( err => {
      console.log(err);
    })
  }



 
    return (
      <div className="animated fadeIn">
        <Container>
          <Row >
            <Col xs="12" sm="6" >
              <Card className="mx-4">
                <CardBody className="p-4">
                    <h1>Registro Carro</h1>
                    <p className="text-muted">Ingresar carro</p>
                    <Form onSubmit={registrar}>  
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setmodelo(e.target.value)} value={modelo} id="modelo" type="text" placeholder="Modelo" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-address-book"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setano(e.target.value)} value={ano} id="ano" type="number" placeholder="Año" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Color</InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setcolor(e.target.value)} value={color} id="color" type="text" placeholder="Color" autoComplete="email" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Serial motor</InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setserial_motor(e.target.value)} value={serial_motor} id="color" type="text" placeholder="Serial motor" autoComplete="email" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-address-card"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setserial_chasis(e.target.value)} value={serial_chasis} id="serial_chasis" type="text" placeholder="Serial Chasis" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>Ciudad</InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setciudad(e.target.value)} value={ciudad} id="color" type="text" placeholder="Ciudad" autoComplete="email" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-volume-control-phone"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setplaca(e.target.value)} value={placa} type="text" placeholder="Placa" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-building"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setdescripcion(e.target.value)} value={descripcion} type="text" placeholder="Descripción" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-building"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setimg(e.target.value)} value={img} type="text" placeholder="img" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-building"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setid_marca(e.target.value)} value={id_marca} type="select" placeholder="Marca" autoComplete="username">
                      {marcas.map(marcas => (
                       <option value={marcas.id_marca}>{marcas.marca}</option>
                      ))}
                        </Input>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="fa fa-building"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setpropietario(e.target.value)} value={propietario} type="text" placeholder="Propietario" autoComplete="username" >

                      </Input>
                    </InputGroup>
                    
                    <Button type="submit" color="success" block>Guardar Persona</Button>
                  </Form>
                </CardBody>
            
              </Card>
            </Col>
          </Row>
        </Container>
       {/*  <Row>
          <Col xs="12" sm="6">
            <CardHeader>
              <strong>Regitrar Carro</strong>
            </CardHeader>
            <CardBody>
            <Form onSubmit={registrar}>
              <Row>
              <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="modelo">Modelo</Label>
                    <Input onChange={e => setmodelo(e.target.value)} value={modelo} type="text" id="modelo"  />
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup >
                    <Label htmlFor="año">Año</Label>
                    <Input onChange={e => setano(e.target.value)} value={ano} type="number" id="año"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup >
                    <Label htmlFor="color">Color</Label>
                    <Input onChange={e => setcolor(e.target.value)} value={color}  type="text" id="color"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup >
                    <Label htmlFor="serial_motor">Serial Motor</Label>
                    <Input onChange={e => setserial_motor(e.target.value)} value={serial_motor} type="number" id="serial_motor"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup >
                    <Label htmlFor="serial_chasis">Serial Chasis</Label>
                    <Input onChange={e => setserial_chasis(e.target.value)} value={serial_chasis} type="number" id="serial_chasis"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup >
                    <Label htmlFor="ciudad">Ciudad</Label>
                    <Input onChange={e => setciudad(e.target.value)} value={ciudad} type="text" id="ciudad"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup >
                    <Label htmlFor="placa">Placa</Label>
                    <Input onChange={e => setplaca(e.target.value)} value={placa} type="text" id="placa"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup >
                    <Label htmlFor="descripcion">Descripcion</Label>
                    <Input onChange={e => setdescripcion(e.target.value)} value={descripcion} type="text" id="descripcion"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup >
                    <Label htmlFor="img">Img</Label>
                    <Input onChange={e => setimg(e.target.value)} value={img} type="text" id="img"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup >
                    <Label htmlFor="id_marca">Marca</Label>
                    <Input onChange={e => setid_marca(e.target.value)} value={id_marca} type="select" id="id_marca">
                    {marcas.map(marcas => (
                       <option value={marcas.id_marca}>{marcas.marca}</option>
                     ))}
                    </Input>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup >
                    <Label htmlFor="propietario">Propietario</Label>
                    <Input onChange={e => setpropietario(e.target.value)} value={propietario} type="text" id="propietario"/>
                  </FormGroup>
                </Col>
              </Row>
              <CardFooter>
              <Button type="submit" size="" color="primary"><i className="fa fa-dot-circle-o"></i>Enviar Formulario</Button>
            </CardFooter>
            </Form>
            </CardBody>
          </Col>
        </Row> */}
     
      </div>

    );
  
}

export default Ingreso_Carro;