import React from 'react';
import ReactDOM from 'react-dom';
import Publicaciones from './Publicaciones';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Publicaciones />, div);
  ReactDOM.unmountComponentAtNode(div);
});
