import React, { useState,useEffect } from 'react';
import {  Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import {carroService} from '../../service/Carro.service';
import Carros from './Carros';

const Publicaciones = () => {

    const[carros,setCarros] = useState([]);

    const loadCarros = async () => {
        carroService.getAll().then(carros => {
          setCarros(carros);
        });
        console.log(carros);
      }

      useEffect( () => {
        loadCarros();
      }, [])
  
   
        return(
            <div className="animated fadeIn">
                <Row>
                    <Col xs="12" lg="12">
                        <Card>
                            <CardHeader>
                                <i className="fa fa-align-justify"></i>Carros Publicados
                            </CardHeader>
                            <CardBody>
                                <Table responsive>
                                   <thead>
                                       <tr>
                                       <th> Precio Venta</th>
                                           <th> Socio</th>
                                           <th>Modelo Carro</th>
                                           <th>Año</th>
                                           <th>Color</th>
                                           <th>Marca</th>
                                           <th>Propietario</th>
                                           <th>Opciones</th>
                                       </tr>
                                   </thead>
                                   <tbody>
                                       {
                                          carros.map( carrito => (
                                            <Carros
                                            key = {carrito.id_en_venta}
                                            carrito = {carrito}
                                            />
                                               
                                          ))
                                            
                                       }
                                      
                                   </tbody>
                                </Table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    
}

export default Publicaciones;