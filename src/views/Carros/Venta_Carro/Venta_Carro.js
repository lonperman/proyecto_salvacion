import React, { useState,useEffect } from 'react';
import {carroService} from '../../service/Carro.service';
import {personaService} from '../../service/Persona.service';
import {
  Button, Card, CardBody, CardFooter, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row
 } from 'reactstrap';

const Venta_Compra = () => {

 
  const[carros,setCarros] = useState([]);
  const[personas,setPersonas] = useState([]);
  const[precioVenta,setprecioVenta] = useState([]);
  const[ganancia,setganancia] = useState([]);
  const[id_carro,setid_carro] = useState([]);
  const[antiguo_d,setantiguo_d] = useState([]);
  const[socio,setsocio] = useState([]);
  const[nuevo_D,setnuevo_D] = useState([]);
  const[id_venta,setid_venta] = useState([]);
  const[co_ventas,setCo_ventas] = useState([]);


  const loadCarros = async () => {
      carroService.getCarros().then(carros => {
        setCarros(carros);
      });
      console.log(carros);
    }

    useEffect( () => {
      loadCarros();
    }, [])

  
 

    const loadPersonas = async () => {
      personaService.getAll().then(personas => {
        setPersonas(personas);
      });
      console.log(personas);
    }

    useEffect( () => {
      loadPersonas();
    }, [])

    const loadId_venta = async () => {
      carroService.getAll().then(id_venta => {
        setid_venta(id_venta);
      });
      console.log(id_venta);
    }

    useEffect( () => {
      loadId_venta();
    }, [])

  const registrar_venta = async() => {
    carroService.postVenta_C(precioVenta,ganancia,id_carro,antiguo_d,socio,nuevo_D,co_ventas).then(carros => {
      setCarros(carros);
    });
    console.log(carros);
  }
    console.log("dato id" + id_venta);
 
  

    return (
      
      <div className="animated fadeIn">
        <Container>
          <Row >
            <Col xs="12" sm="6" >
              <Card className="mx-4">
                <CardBody className="p-4">
                    <h1>Registro venta</h1>
                    <p className="text-muted">Registrar venta</p>
                    <Form onSubmit={registrar_venta}>  
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i class="fa fa-money" aria-hidden="true"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setprecioVenta(e.target.value)} value={precioVenta} type="text" id="precio_venta" placeholder="Precio de venta" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i class="fa fa-money" aria-hidden="true"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setganancia(e.target.value)} value={ganancia} type="number" id="ganancias" placeholder="Ganancias" autoComplete="username" />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                      <i>Placa</i>
                      </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setid_carro(e.target.value)} value={id_carro} type="select" id="id_carro" placeholder="Selecciona la placa" >
                      {carros.map(carrito => (
                       <option value={carrito.id_carro}>{carrito.placa}</option>  
                     ))}
                     </Input>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        
                        <InputGroupText>
                          <i>Antiguo Dueño</i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setantiguo_d(e.target.value)} value={antiguo_d} type="select" id="antiguo_dueño" placeholder="Tipo Documento" autoComplete="username" >
                      {carros.map(carrito => (
                      
                      <option value={carrito.propietario_persona.id_persona}  key = {carrito.id_carro}
                      >{carrito.propietario_persona.nombre_completo}</option>
                    ))}
                        </Input>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i>Socio</i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setsocio(e.target.value)} value={socio} type="select" id="socio" placeholder="Telefono" autoComplete="username" >
                      {personas.map(persona => (
                       <option value={persona.id_persona}>{persona.nombre_completo}</option>
                     ))}
                        </Input>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i>Nuevo Dueño</i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setnuevo_D(e.target.value)} value={nuevo_D}  type="select" id="nuevo_dueño"placeholder="Direccion" autoComplete="username" >
                      {personas.map(persona => (
                       <option value={persona.id_persona}>{persona.nombre_completo}</option>
                     ))}
                        </Input>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i class="fa fa-money" aria-hidden="true"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input onChange={e => setCo_ventas(e.target.value)} value={co_ventas}  type="select" id="nuevo_dueño"placeholder="Direccion" autoComplete="username">
                        {id_venta.map(id => (
                          <option value={id.id_en_venta}>{id.carro.modelo}</option>
                        ))}
                      </Input>
                    </InputGroup>
                    
                    <Button type="submit" color="success" block>Guardar Venta</Button>
                  </Form>
                </CardBody>
            
              </Card>
            </Col>
          </Row>
        </Container>
        {/* <Row>
          <Col xs="12" sm="61">
            <CardHeader>
              <strong>Formulario De Venta</strong>
            </CardHeader>
            <CardBody>
              <Form onSubmit={registrar_venta}>
              <Row>
              <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="precio_venta">Precio De Venta</Label>
                    <Input onChange={e => setprecioVenta(e.target.value)} value={precioVenta} type="number" id="precio_venta"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="ganancia">Ganancia</Label>
                    <Input onChange={e => setganancia(e.target.value)} value={ganancia} type="number" id="ganancia"/>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="id_carro">Placa</Label>
                    <Input onChange={e => setid_carro(e.target.value)} value={id_carro} type="select" id="id_carro">
                     {carros.map(carrito => (
                       <option value={carrito.id_carro}>{carrito.placa}</option>  
                     ))}
                    </Input>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="antiguo_daño">Antiguo Dueño</Label>
                    <Input onChange={e => setantiguo_d(e.target.value)} value={antiguo_d} type="select" id="antiguo_dueño">
                    {carros.map(carrito => (
                      
                       <option value={carrito.propietario_persona.id_persona}  key = {carrito.id_carro}
                       >{carrito.propietario_persona.nombre_completo}</option>
                     ))}
                    </Input>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="socio">Socio</Label>
                    <Input onChange={e => setsocio(e.target.value)} value={socio}  type="select" id="socio">
                    {personas.map(persona => (
                       <option value={persona.id_persona}>{persona.nombre_completo}</option>
                     ))}
                    </Input>
                  </FormGroup>
                </Col>
                <Col xs="12">
                  <FormGroup>
                    <Label htmlFor="nuevo_dueño">Nuevo Dueño</Label>
                    <Input onChange={e => setnuevo_D(e.target.value)} value={nuevo_D} type="select" id="nuevo_dueño">
                    {personas.map(persona => (
                       <option value={persona.id_persona}>{persona.nombre_completo}</option>
                     ))}
                    </Input>
                  </FormGroup>
                </Col>
                <CardFooter>
              <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i>Submit</Button>
            </CardFooter>
              </Row>
              </Form>
            </CardBody>
           
          </Col>
        </Row> */}
     
      </div>

    );
  }


export default Venta_Compra;