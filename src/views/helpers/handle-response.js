import authenticationService from '../service/authentication.service';

export function handleResponse(response) {
    if(!response.status){
        if([401,403].indexOf(response.status) !== -1){
            authenticationService.logout();
           
        }

        const error = (response.data && response.data.message) || response.statusText;
        return Promise.reject(error);
    }
    return response.data;
}
